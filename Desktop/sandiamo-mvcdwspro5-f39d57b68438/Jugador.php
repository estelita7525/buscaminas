<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Jugador {

//Propiedades
    private $id;
    private $nombre;
    private $division;
    private $equipo;

//Constructor

    public function __construct($id, $nombre, $division, $equipo) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->division = $division;
        $this->equipo = $equipo;
    }

//Metodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getDivision() {
        return $this->division;
    }

    public function setDivision($division) {
        $this->division = $division;
    }

    public function getEquipo() {
        return $this->equipo;
    }

    public function setEquipo($equipo) {
        $this->equipo = $equipo;
    }

}

?>
