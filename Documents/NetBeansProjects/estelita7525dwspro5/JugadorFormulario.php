<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Jugador.php");
        include_once("Modelo.php");
        include_once("funciones.php");
        ?>

        <h1>Datos del Asignatura</h1>
        <h1>Alta Asignatura</h1>
        <form method="POST" action="JugadorGrabar.php" >
            <table border="1">
                <tr>
                    <td>Id:</td><td><input type="text" name="id" /></td>
                </tr>
                <tr>
                    <td>Nombre:</td><td><input type="text" name="nombre" /></td>
                </tr>
                <tr>
                    <td>Division</td><td><input type="text" name="division" /></td>
                </tr>

                <tr>
                    <td>Jugador</td><td>
                        <?php
                        $modelo = new Modelo();
                        $profesores = array();
                        $jugadores = $modelo->jugadores();
                        echo '<select name="jugador">';
                        foreach ($jugadores as $jugador) {
                            $id = $jugador->getId();
                            $nombre = $jugador->getNombre();
                            echo "<option value='" . $id . "'>" . $nombre . "</option>";
                        }
                        echo "</select>";
                        ?>
                </tr>

                <tr>
                    <td><input type="submit"  name= "Enviar" value="Enviar"></td>
                    <td><input type="reset"   name= "Borrar" value="Borrar"></td>
                </tr>
            </table>
        </form>
        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>