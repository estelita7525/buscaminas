
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");
        include_once("funciones.php");

        function recoge($campo) {
            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            }
            return $valor;
        }

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $equipo = new Equipo($id, $nombre);
            return $equipo;
        }

        //***************************
        //* Main
        //***************************

        $profesor = leer();

        if ($equipo->getId() != "" && $equipo->getNombre() != "") {

            $modelo = new Modelo();
            $modelo->grabarEquipo($equipo);

            echo "Grabado: " . $equipo->getNombre() . "<br>";
        } else {
            echo "Error: Campos vacios" . "<br>";
        }



        inicio();
        pie();
        ?>
    </body>
</html>
