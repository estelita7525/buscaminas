
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("Modelo.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Jugador:</p>
        <ul>
            <li><a href="JugadorFormulario.php">Alta </a> </li>
        </ul>

        <?php
        $modelo = new Modelo();
        $jugadores = $modelo->getJugadores();

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Asignatura</td>';
        echo '<td>Nombre</td>';
        echo '<td>Horas</td>';
        echo '<td>Id Profesor</td>';
        echo '</tr>';

        echo "<p>Listado:</p>";

        foreach ($jugadores as $jugador) {

            echo "<tr>";
            echo "<td>" . $jugador->getId() . "</td>";
            echo "<td>" . $jugador->getNombre() . "</td>";
            echo "<td>" . $jugador->getDivision() . "</td>";
            echo "<td>" . $jugador->getEquipo()->getId() . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "<br/>";
        ?>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>
