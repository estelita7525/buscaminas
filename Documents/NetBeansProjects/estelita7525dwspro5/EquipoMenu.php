
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("Modelo.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Equipo:</p>
        <ul>
            <li><a href="EquipoFormulario.html">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Prof</td>';
        echo '<td>Nom Prof</td>';
        echo '</tr>';

        $modelo = new Modelo();
        $equipos = $modelo->getEquipos();

        if (count($equipos) > 0) {

            foreach ($equipos as $equipo) {
                echo "<tr>";
                echo "<td>" . $equipo->getId() . "</td>";
                echo "<td>" . $equipo->getNombre() . "</td>";
                echo "</tr>";
            }
        }

        echo "</table>";
        echo "<br/>";
        ?>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>
