<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Equipo.php");
include_once("Jugador.php");
include_once("Modelo.php");

class Modelo {

    private $fe = "Equipo.txt";
    private $fj = "Jugador.txt";

    //**************************************
    public function getJugadores() {
        $jugadores = array();

        $f = @fopen($this->fp, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $jugador = new Jugador($data[0], $data[1]);
                $equipos[$cont] = $equipo;
                $cont++;
                $data = fgetcsv($f, 1000, ";");
            }
            fclose($f);

            //echo "Num equipos " . count($equipos) . "<br>";
            //print_r($equipos);
        } else {
            echo "Error: No se puede abrir: " . $fp . "<br>";
        }

        return $equipos;
    }

    //**************************************
    function getEquipo($equipo_) {
        $f = fopen($this->fp, "r");
        $data = fgetcsv($f, 1000, ";");
        $equipo = new Equipo("", "");

        while ($data) {
            $idp = $data[0];
            echo "Equipo : " . $idp . " ==  " . $equipo_->getId() . "?<br>";
            if ($idp == $profesor_->getId()) {
                $equipo->setId($data[0]);
                $equipo->setNombre($data[1]);
                break;
            }

            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        //echo "Leido Objeto: " . $equipo->getId() . " " . $equipo->getNombre() . "<br>";
        return $equipo;
    }

    //**************************************
    function grabarEquipo($equipo) {

        $f = fopen($this->fp, "a");
        $linea = $equipo->getId()
          . ";" . $equipo->getNombre()
          . "\r\n";
        fwrite($f, $linea);

        fclose($f);
    }

    //**************************************

    function grabarJugador($jugador) {
        $f = fopen($this->fa, "a");
        $linea = $jugador->getId()
          . ";" . $jugador->getNombre()
          . ";" . $jugador->getDivision()
          . ";" . $jugador->getEquipo()->getId()
          . "\r\n";
        fwrite($f, $linea);
        fclose($f);
    }

    //**************************************
    public function getJugadores() {
        $jugadores = array();
        $f = fopen($this->fa, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $equipo = new Equipo($data[2], "");
                $jugador = new jugador($data[0], $data[1], $data[2], $equipo);
                $data = fgetcsv($f, 1000, ";");
                $jugadores[$cont] = $jugador;
                $cont++;
            }
            //echo "Jugadores " . count($jugadores);
            fclose($f);
        } else {
            echo "Error: No se puede abrir: " . $this->fa . "<br>";
        }

        return $jugadores;
    }

}
