
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");

        //**************************************
        function recoge($campo) {

            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            };
            return $valor;
        }

        //**************************************
        function leerJugador() {

            $id = recoge("id");
            $nombre = recoge("nombre");
            $division = recoge("division");
            $jugadorid = recoge("jugador");


            //echo "Leido form: " . $id . " " . $nombre . " " . $jugadororid . "<br>";

            $jugador_ = new Jugador($jugadorid, "");
            $modelo = new Modelo();
            $jugador = $modelo->getJugador($jugador_);
            $division = new division($id, $nombre, $division, $jugador);



            return $jugador;
        }

        //*****************************
        //*  main
        //*****************************


        $jugador = leerJugador();

        //echo "Leido Asign: " . $jugador->getId() . " " . $jugador->getNombre() . $jugador->getProfesor()->getId() .         "<br>";

        if ($jugador->getId() == "") {
            echo "Error: Id jugador vacio" . "<br>";
        } else if ($jugador->getNombre() == "") {
            echo "Error: Nombre jugador vacio" . "<br>";
        } else if ($jugador->getDivision() == "") {
            echo "Error: Jugador vacio" . "<br>";
        } else if ($jugador->getEquipo()->getId() == "") {
            echo "Error: Nombre equipo vacio" . "<br>";
        } else {
            $modelo = new Modelo();
            $modelo->grabarJugador($jugador);
            echo "Grabado: " . $jugador->getNombre() . "<br>";
        }


        echo "<a href='index.php'>Volver</a>";
        ?>
    </body>
</html>
